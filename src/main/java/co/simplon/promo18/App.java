package co.simplon.promo18;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        int tryNb = 1;
         
        Scanner scan = new Scanner(System.in);
        System.out.println("Faire deviner un mot à un autre joueur");
        String wordToGuess =scan.nextLine();
        
        System.out.print("\033[H\033[2J");
        char[] wordToGuessTab = wordToGuess.toCharArray();
        char[] tabGuessedSoFar = new char[wordToGuessTab.length];
        String wordGuessedSoFar = "";

        for (int i = 0; i < tabGuessedSoFar.length; i++) {
            tabGuessedSoFar[i] = '*';
            wordGuessedSoFar = wordGuessedSoFar + "*";
        }
        
        while (tryNb < 8) {

            System.out.println("Essayez une lettre");
           
            
            String Lettertryed = scan.nextLine();
           
            char charLetter = Lettertryed.charAt(0);
            
            for (int index = 0; index < wordToGuessTab.length; index++) {
                
                if (charLetter == wordToGuessTab[index]) {
                   
                    tabGuessedSoFar[index] = charLetter;

                }
            }
            
            for (int i = 0; i < tabGuessedSoFar.length; i++) {
                
                System.out.print(tabGuessedSoFar[i]);
            }
          
            System.out.println();
            
            int lettersFound = 0;
            
            for (int i = 0; i < tabGuessedSoFar.length; i++) {
                
                if (tabGuessedSoFar[i] != '*') {
                   
                    lettersFound++;
                }
            }
            
            if (lettersFound == wordToGuess.length()) {
                System.out.println("Victoire!!!!!");
            }
      
            if (tryNb == 7) {
                System.out.println("Vous avez perdu");
                
                System.out.println("Le mot à deviner était "+wordToGuess);
            }
            
            tryNb++;

        }

    }

}
